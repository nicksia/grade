﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grades
{
    public class NameChangedEventArgs : EventArgs
    {
        string existingName;
        public string ExistingName
        {
            get { return existingName; }
            set
            {
                existingName = value;
            }
        }

        string newName;
        public string NewName
        {
            get { return newName; }
            set
            {
                newName = value;
            }
        }
        
    }
}
