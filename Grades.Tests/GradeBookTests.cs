﻿using Grades;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grades.Tests
{
    [TestClass]
    public class GradeBookTests
    {
        [TestMethod]
        public void computeHighestGrade()
        {
            GradeBook book = new GradeBook();
            book.addGrade(10);
            book.addGrade(90);

            GradeStatistics result = book.ComputeStatistics();
            Assert.AreEqual(90, result.HighestGrade);
        }

        [TestMethod]
        public void computeLowestGrade()
        {
            GradeBook book = new GradeBook();
            book.addGrade(10);
            book.addGrade(90);

            GradeStatistics result = book.ComputeStatistics();
            Assert.AreEqual(10, result.LowestGrade);
        }

        [TestMethod]
        public void computeAverageGrade()
        {
            GradeBook book = new GradeBook();
            book.addGrade(91);
            book.addGrade(89.5f);
            book.addGrade(75);

            GradeStatistics result = book.ComputeStatistics();
            Assert.AreEqual(85.16f, result.AverageGrade, 0.01);
        }

        [TestMethod]
        public void computeLetterGrade()
        {
            GradeBook book = new GradeBook();
            book.addGrade(91);
            book.addGrade(89.5f);
            book.addGrade(75);

            GradeStatistics result = book.ComputeStatistics();
            Assert.AreEqual("B", result.LetterGrade);
        }
    }
}